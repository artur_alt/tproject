<?php

namespace App\Tests;

use App\Service\ElasticSearchService;
use Elastica\Query;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use FOS\ElasticaBundle\Repository;
use PHPUnit\Framework\TestCase;

class ElasticSearchServiceTest extends TestCase
{
    public function testGetNewCards(): void
    {
        $repository = $this->createMock(Repository::class);
        $repositoryManager = $this->createMock(RepositoryManagerInterface::class);
        $service = new ElasticSearchService($repositoryManager);

        $repositoryManager->expects($this->exactly(1))
            ->method('getRepository')
            ->with('app_card')
            ->willReturn($repository);

        $query = (new Query())
            ->addSort(['createdAt' => ['order' => 'desc']]);

        $result = [];
        $repository->expects($this->exactly(1))
            ->method('find')
            ->with($query)
            ->willReturn($result);

        $this->assertEquals($result, $service->getNewCards(3));
    }
}
