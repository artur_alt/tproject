<?php

namespace App\Tests;

use App\Entity\Card;
use App\Service\CardService;
use DateTime;
use PHPUnit\Framework\TestCase;

class CardServiceTest extends TestCase
{
    public function testConvertToResponse(): void
    {
        $time = new DateTime();
        $card = (new Card())
            ->setName('lol')
            ->setDescription('lol 2')
            ->setViewsCount(10)
            ->setImage('lol.jpg')
            ->setCreatedAt($time)
            ->setUpdatedAt($time);

        $path = '/test';
        $service = new CardService($path);

        $response = $service->convertToResponse($card);

        $this->assertEquals($card->getName(), $response->getName());
        $this->assertEquals($card->getDescription(), $response->getDescription());
        $this->assertEquals($card->getViewsCount(), $response->getViewsCount());
        $this->assertEquals($path . '/' . $card->getImage(), $response->getImage());
        $this->assertEquals($card->getCreatedAt()->getTimestamp(), $response->getCreatedAt()->getTimestamp());
        $this->assertEquals($card->getUpdatedAt()->getTimestamp(), $response->getUpdatedAt()->getTimestamp());
    }
}
