<?php


namespace App\Service;


use Exception;

class ImageService
{
    /**
     * @var string
     */
    private $imageDir;

    /**
     * ImageService constructor.
     * @param string $imageDir
     */
    public function __construct(string $imageDir)
    {
        $this->imageDir = $imageDir;
    }

    /**
     * @param string $data
     *
     * @return string
     *
     * @throws Exception
     */
    public function uploadFromBase64(string $data): string
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new Exception('Invalid image type');
            }
            $data = str_replace( ' ', '+', $data );
            $data = base64_decode($data);

            if (false === $data) {
                throw new Exception('base64_decode failed');
            }
        } else {
            throw new Exception('Did not match data URI with image data');
        }

        $fileName = sprintf('%s.%s', uniqid(), $type);
        file_put_contents(sprintf('%s/%s', $this->imageDir, $fileName), $data);

        return $fileName;
    }

    /**
     * @param string $imageName
     */
    public function deleteImage(string $imageName): void
    {
        unlink($this->imageDir . '/' . $imageName);
    }
}
