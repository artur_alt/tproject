<?php


namespace App\Service;


use Elastica\Query;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;

class ElasticSearchService
{
    private const CARD_INDEX_NAME = 'app_card';
    private const DEFAULT_CARD_LIMIT = 6;

    /**
     * @var RepositoryManagerInterface
     */
    private $repositoryManager;

    public function __construct(RepositoryManagerInterface $repositoryManager)
    {
        $this->repositoryManager = $repositoryManager;
    }

    public function getNewCards(int $limit = self::DEFAULT_CARD_LIMIT): array
    {
        $repository = $this->repositoryManager->getRepository(self::CARD_INDEX_NAME);

        $query = (new Query())
            ->addSort(['createdAt' => ['order' => 'desc']]);

        return $repository->find($query, $limit);
    }
}
