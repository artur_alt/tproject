<?php


namespace App\Service;


use App\Dto\Response\CardResponse;
use App\Entity\Card;

class CardService
{
    /**
     * @var string
     */
    private $imageDir;

    /**
     * CardService constructor.
     * @param string $imageDir
     */
    public function __construct(string $imageDir)
    {
        $this->imageDir = $imageDir;
    }

    /**
     * @param Card $card
     *
     * @return CardResponse
     */
    public function convertToResponse(Card $card): CardResponse
    {
        return (new CardResponse())
            ->setId($card->getId())
            ->setName($card->getName())
            ->setDescription($card->getDescription())
            ->setImage($this->imageDir . '/' . $card->getImage())
            ->setViewsCount($card->getViewsCount())
            ->setCreatedAt($card->getCreatedAt())
            ->setUpdatedAt($card->getUpdatedAt());
    }
}
