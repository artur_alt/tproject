<?php


namespace App\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateCardRequest implements RequestInterface
{
    /**
     * @var int|null
     *
     * @Assert\Type("integer")
     * @Assert\NotBlank
     *
     * @Serializer\Type("integer")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $name;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $description;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("imageBase64")
     */
    private $imageBase64;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageBase64(): ?string
    {
        return $this->imageBase64;
    }

    /**
     * @param string|null $imageBase64
     *
     * @return $this
     */
    public function setImageBase64(?string $imageBase64): self
    {
        $this->imageBase64 = $imageBase64;

        return $this;
    }
}
