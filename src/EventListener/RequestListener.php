<?php


namespace App\EventListener;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestListener
{
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        if ($request->getMethod() === Request::METHOD_POST) {
            /** @noinspection PhpComposerExtensionStubsInspection */
            $rowData = json_decode(file_get_contents("php://input"), true);

            if (is_array($rowData)) {
                $request->request->add($rowData);
            }
        }
    }
}
