<?php

namespace App\Controller;

use App\Dto\Response\ErrorDto;
use App\Dto\Response\ErrorResponse;
use App\Entity\Card;
use App\Service\CardService;
use App\Service\ElasticSearchService;
use Doctrine\Persistence\ManagerRegistry;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends AbstractController
{
    /**
     * @var ArrayTransformerInterface
     */
    private $arrayTransformer;

    /**
     * @var ElasticSearchService
     */
    private $elasticSearchService;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var CardService
     */
    private $cardService;

    public function __construct(
        ElasticSearchService $elasticSearchService,
        ArrayTransformerInterface $arrayTransformer,
        ManagerRegistry $managerRegistry,
        CardService $cardService
    )
    {
        $this->elasticSearchService = $elasticSearchService;
        $this->arrayTransformer = $arrayTransformer;
        $this->managerRegistry = $managerRegistry;
        $this->cardService = $cardService;
    }

    /**
     * @Route("/card", name="card", methods={"GET"})
     *
     * @return Response
     */
    public function index(): Response
    {
        $cards = $this->elasticSearchService->getNewCards();

        return $this->json($this->arrayTransformer->toArray(array_map(function(Card $card) {
            return $this->cardService->convertToResponse($card);
        }, $cards)));
    }

    /**
     * @Route("/card/{id}", name="get_card", methods={"GET"})
     *
     * @param int $id
     *
     * @return Response
     */
    public function getById(int $id): Response
    {
        $card = $this->managerRegistry->getRepository(Card::class)->find($id);

        if (null === $card) {
            return $this->json($this->arrayTransformer->toArray((new ErrorResponse())->setErrors([
                (new ErrorDto())->setMessage(sprintf('Card #%d not found', $id))
            ])),Response::HTTP_NOT_FOUND);
        }

        $card->setViewsCount($card->getViewsCount() + 1);
        $this->managerRegistry->getManager()->persist($card);
        $this->managerRegistry->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray(
            $this->cardService->convertToResponse($card)
        ));
    }
}
