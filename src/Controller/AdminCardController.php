<?php


namespace App\Controller;


use App\Dto\Request\CreateCardRequest;
use App\Dto\Request\RequestInterface;
use App\Dto\Request\UpdateCardRequest;
use App\Dto\Response\ErrorDto;
use App\Dto\Response\ErrorResponse;
use App\Entity\Card;
use App\Service\CardService;
use App\Service\ImageService;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AdminCardController
 * @package App\Controller
 *
 * @Route("/admin/card")
 */
class AdminCardController extends AbstractController
{
    public const AUTH_HEADER_NAME = 'X-Secret-Key';

    /**
     * @var ArrayTransformerInterface
     */
    private $arrayTransformer;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * @var CardService
     */
    private $cardService;

    /**
     * @var string
     */
    private $apiSecretKey;

    public function __construct(
        ArrayTransformerInterface $arrayTransformer,
        ManagerRegistry $managerRegistry,
        ValidatorInterface $validator,
        ImageService $imageService,
        CardService $cardService,
        string $apiSecretKey
    )
    {
        $this->arrayTransformer = $arrayTransformer;
        $this->managerRegistry = $managerRegistry;
        $this->validator = $validator;
        $this->imageService = $imageService;
        $this->cardService = $cardService;
        $this->apiSecretKey = $apiSecretKey;
    }

    /**
     * @Route("/create", name="card_create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        if ($request->headers->get(self::AUTH_HEADER_NAME) !== $this->apiSecretKey) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var CreateCardRequest $createCardRequest */
        $createCardRequest = $this->arrayTransformer->fromArray($request->request->all(), CreateCardRequest::class);
        $errorResponse = $this->validateRequest($createCardRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        try {
            $fileName = $this->imageService->uploadFromBase64($createCardRequest->getImageBase64());
        } catch (\Exception $e) {
            return $this->json($this->arrayTransformer->toArray((new ErrorResponse())->setErrors([
                (new ErrorDto())->setMessage($e->getMessage())
            ])),Response::HTTP_NOT_FOUND);
        }

        $card = (new Card)
            ->setName($createCardRequest->getName())
            ->setDescription($createCardRequest->getDescription())
            ->setViewsCount(0)
            ->setImage($fileName)
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->managerRegistry->getManager()->persist($card);
        $this->managerRegistry->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray(
            $this->cardService->convertToResponse($card)
        ));
    }

    /**
     * @Route("/update", name="card_update", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request): Response
    {
        if ($request->headers->get(self::AUTH_HEADER_NAME) !== $this->apiSecretKey) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var UpdateCardRequest $updateCardRequest */
        $updateCardRequest = $this->arrayTransformer->fromArray($request->request->all(), UpdateCardRequest::class);
        $errorResponse = $this->validateRequest($updateCardRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $card = $this->managerRegistry->getRepository(Card::class)->find($updateCardRequest->getId());

        if (null === $card) {
            return $this->json($this->arrayTransformer->toArray((new ErrorResponse())->setErrors([
                (new ErrorDto())->setMessage(sprintf('Card #%d not found', $updateCardRequest->getId()))
            ])),Response::HTTP_NOT_FOUND);
        }

        if (null !== $updateCardRequest->getImageBase64() && '' !== trim($updateCardRequest->getImageBase64())) {
            try {
                $fileName = $this->imageService->uploadFromBase64($updateCardRequest->getImageBase64());
                $this->imageService->deleteImage($card->getImage());
                $card->setImage($fileName);
            } catch (\Exception $e) {
                return $this->json($this->arrayTransformer->toArray((new ErrorResponse())->setErrors([
                    (new ErrorDto())->setMessage($e->getMessage())
                ])),Response::HTTP_NOT_FOUND);
            }
        }
        if (null !== $updateCardRequest->getName() && '' !== trim($updateCardRequest->getName())) {
            $card->setName($updateCardRequest->getName());
        }
        if (null !== $updateCardRequest->getDescription() && '' !== trim($updateCardRequest->getDescription())) {
            $card->setDescription($updateCardRequest->getDescription());
        }
        $card->setUpdatedAt(new DateTime());

        $this->managerRegistry->getManager()->persist($card);
        $this->managerRegistry->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray(
            $this->cardService->convertToResponse($card)
        ));
    }

    /**
     * @Route("/delete/{id}", name="card_delete", methods={"POST"})
     *
     * @param int $id
     * @param Request $request
     *
     * @return Response
     */
    public function delete(int $id, Request $request): Response
    {
        if ($request->headers->get(self::AUTH_HEADER_NAME) !== $this->apiSecretKey) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        $card = $this->managerRegistry->getRepository(Card::class)->find($id);

        if (null === $card) {
            return $this->json($this->arrayTransformer->toArray((new ErrorResponse())->setErrors([
                (new ErrorDto())->setMessage(sprintf('Card #%d not found', $id))
            ])),Response::HTTP_NOT_FOUND);
        }

        $this->managerRegistry->getManager()->remove($card);
        $this->managerRegistry->getManager()->flush();

        if ('' !== trim($card->getImage())) {
            try {
                $this->imageService->deleteImage($card->getImage());
            } catch (\Exception $e) {
                return $this->json($this->arrayTransformer->toArray((new ErrorResponse())->setErrors([
                    (new ErrorDto())->setMessage($e->getMessage())
                ])), Response::HTTP_NOT_FOUND);
            }
        }

        return new Response();
    }

    /**
     * @param RequestInterface $request
     * @param string|GroupSequence|(string|GroupSequence)[]|null $groups
     *
     * @return ErrorResponse|null
     */
    private function validateRequest(RequestInterface $request, $groups = null): ?ErrorResponse
    {
        $errors = $this->validator->validate($request, null, $groups);
        if (0 === $errors->count()) {
            return null;
        }

        $errorList = [];
        foreach ($errors as $violation) {
            $errorList[] = (new ErrorDto())
                ->setMessage(sprintf('`%s` %s', $violation->getPropertyPath(), $violation->getMessage()));
        }

        return (new ErrorResponse())->setErrors($errorList);
    }
}
